<?php

require 'recipe/laravel.php';

server('dev', 'IP_')
    ->user('root')
    ->password('GLUGLU')
    ->env('deploy_path', '/var/www/html')
    ->stage('production');

set('repository', 'repo.git');

// Laravel shared dirs
set('shared_dirs', [
    'storage/app',
    'storage/framework/cache',
    'storage/framework/sessions',
    'storage/framework/views',
    'storage/logs',
]);

// Laravel 5 shared file
set('shared_files', ['.env']);

// Laravel writable dirs
set('writable_dirs', ['storage', 'vendor']);

task('composer:install', function () {
     run('cd /var/www/html/current/ && composer install -o --no-dev');
});

task('migrations', function () {
     run('cd /var/www/html/current/ && php artisan migrate');
});

task('generate_key', function () {
  run('cd /var/www/html/current/ && php artisan key:generate');
});

task('dumpautoload', function () {
  run('cd /var/www/html/current/ && composer dumpautoload');
});

task('setGroup', function () {
    run('cd /var/www/ && chown -R www-data:www-data html');
});

task('setFolderPerm', function () {
    run('cd /var/www/html/ && chmod -R 775 shared');
});

task('git:pull', function () {
    run('cd /var/www/html/current/ && git pull origin master');
});


after('deploy', 'composer:install');
after('deploy', 'generate_key');
after('deploy', 'dumpautoload');
after('deploy', 'setGroup');
after('deploy', 'setFolderPerm');
after('deploy', 'migrations');
after('deploy', 'git:pull');


